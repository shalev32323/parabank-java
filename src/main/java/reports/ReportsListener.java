package reports;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentSparkReporter;
import com.aventstack.extentreports.reporter.configuration.Theme;
import org.testng.*;
import reports.ScreenShotGenerate;
import utils.BaseTest;

import java.util.Map;

public class ReportsListener implements ITestListener, IConfigurationListener {

    private ExtentSparkReporter spark = new ExtentSparkReporter(System.getProperty("user.dir") + "/src/main/java/reports/Report.html");
    public static ExtentReports extent = new ExtentReports();
    private static ExtentTest node;

    public static ExtentTest getNode() {
        return node;
    }

    @Override
    public void beforeConfiguration(ITestResult tr, ITestNGMethod tm) {
        if (tr.getMethod().isBeforeMethodConfiguration()) {
            node = BaseTest.test.createNode(tr.getName());
        }
    }

    @Override
    public synchronized void onTestStart(ITestResult result) {
        node = BaseTest.test.createNode(result.getName());
    }

    @Override
    public synchronized  void onTestSuccess(ITestResult result) {
    }

    @Override
    public synchronized  void onTestFailure(ITestResult result) {
        node.fail(result.getThrowable(),
                MediaEntityBuilder.createScreenCaptureFromPath("./screenshots/"
                        + ScreenShotGenerate.takeScreenShot(result.getName() + "_")).build());
    }

    @Override
    public synchronized  void onTestSkipped(ITestResult result) {
        node.skip(result.getThrowable());
    }

    @Override
    public  synchronized void onTestFailedButWithinSuccessPercentage(ITestResult result) {
    }

    @Override
    public  synchronized void onStart(ITestContext context) {
        spark.config().setDocumentTitle("Report");
        spark.config().setReportName("shalev");
        spark.config().setTheme(Theme.DARK);
        extent.attachReporter(spark);
    }

    @Override
    public synchronized  void onFinish(ITestContext context) {
        extent.flush();
    }
}

