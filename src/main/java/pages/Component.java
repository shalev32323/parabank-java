package pages;

import org.openqa.selenium.SearchContext;
import org.openqa.selenium.support.PageFactory;

public class Component <T extends SearchContext> {
    protected final T ROOT;

    public Component (T root) {
        this.ROOT = root;
        PageFactory.initElements(root, this);
    }

}
