package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;;

public class HomePage extends BasePage {
    @FindBy(css = "[href='/parabank/openaccount.htm']")
    private WebElement openNewAccount;

    @FindBy(css = "[href='/parabank/requestloan.htm']")
    private WebElement openRequestLoan;

    @FindBy(css = "[href='/parabank/billpay.htm']")
    private WebElement openBillPay;

    @FindBy(css = "[href='/parabank/logout.htm']")
    private WebElement LogOutBtn;

    @FindBy(css = "[href='/parabank/overview.htm']")
    private WebElement openOverView;

    @FindBy(css = "[href='/parabank/transfer.htm']")
    private WebElement openTransferFunds;

    @FindBy(className = "title")
    private WebElement pageTitle;

    @FindBy(css = "#leftPanel h2")
    private WebElement panelTitle;

    public HomePage(WebDriver driver){
        super(driver);
    }

    public String getPageTitle() {
        return super.getElementText(this.pageTitle);
    }

    public String getPanelTitle() {
        return super.getElementText(this.panelTitle);
    }

    public OpenNewAccountPage clickOpenNewAccount() {
        super.click(this.openNewAccount, " open new account page");
        return new OpenNewAccountPage(driver);
    }

    public TransferFundsPage clickTransferFunds() {
        super.click(this.openTransferFunds, "transfer funds");
        return new TransferFundsPage(driver);
    }

    public LoginPage clickLogOut() {
        super.click(this.LogOutBtn, "log out");
        return new LoginPage(driver);
    }

    public AccountOverView clickAccountOverview() {
        super.click(this.openOverView, "account overview");
        return new AccountOverView(driver);
    }
    public LoanRequestPage openLoanRequest() {
        super.click(this.openRequestLoan, " open new account page");
        return new LoanRequestPage(driver);
    }
}
