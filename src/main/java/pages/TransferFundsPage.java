package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class TransferFundsPage extends BasePage {
    @FindBy(id = "amount")
    private WebElement amountField;

    @FindBy(id = "fromAccountId")
    private WebElement fromAccountSelect;

    @FindBy(id = "toAccountId")
    private WebElement toAccountSelect;

    @FindBy(css = "[type = 'submit'][value= 'Transfer']")
    private WebElement transferBtn;

    public TransferFundsPage(WebDriver driver) {
        super(driver);
    }

    public void setAmountField (String amount) {
        super.sendKeys(this.amountField, amount);
    }

    public void chooseFromAccount (String value) {
        super.chooseSelectValue(this.fromAccountSelect, value);
    }

    public void chooseToAccount (String value) {
        super.chooseSelectValue(this.toAccountSelect, value);
    }

    public void clickTransfer () {
        super.click(this.transferBtn, "transfer");
    }
}
