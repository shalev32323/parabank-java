package pages;

import com.aventstack.extentreports.Status;
import enums.RegisterFields;
import interfaces.IFormPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import reports.ReportsListener;

public class RegisterPage extends BasePage implements IFormPage<RegisterFields> {
    @FindBy(css = "[type = 'submit'][value= 'Register']")
    private WebElement registerBtn;

    @FindBy(css = "#rightPanel p")
    private WebElement successMessage;

    @FindBy(className = "title")
    private WebElement pageTitle;

    public RegisterPage(WebDriver driver) {
        super(driver);
    }

    public String getErrorMessage(RegisterFields registerFieldsName) {
        String locator = registerFieldsName.fieldName + ".errors";
        return super.wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(locator))).getText();
    }

    public String getPageTitle() {
        return super.getElementText(this.pageTitle);
    }

    public String getSuccessMessage() {
        return super.getElementText(this.successMessage);
    }

    public HomePage clickRegister() {
        super.click(this.registerBtn, "register button");
        return new HomePage(driver);
    }

    @Override
    public void clearField(RegisterFields registerFields) {
        wait.until(ExpectedConditions.elementToBeClickable(By.id(registerFields.fieldName))).clear();
        ReportsListener.getNode().log
                (Status.PASS, "clear " + registerFields.name() + " field");
    }

    @Override
    public void fillField(RegisterFields key, String value) {
        wait.until(ExpectedConditions.elementToBeClickable(By.id(key.fieldName))).sendKeys(value);
        ReportsListener.getNode().log
                (Status.PASS, "insert " + value + " to " + key.name() + " field");
    }

    public void clickRegisterBtn() {
        super.click(this.registerBtn, "register button");
    }

}
