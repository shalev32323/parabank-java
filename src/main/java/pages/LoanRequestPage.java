package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class LoanRequestPage extends BasePage {
    @FindBy(id = "amount")
    private WebElement loanAmountField;

    @FindBy(id = "downPayment")
    private WebElement downPaymentField;

    @FindBy(id = "fromAccountId option")
    private WebElement fromAccountSelect;

    @FindBy(css = "[type = 'submit'][value= 'Apply Now']")
    private WebElement applyNowBtn;

    public LoanRequestPage(WebDriver driver) {
        super(driver);
    }

    public void setLoanAmountField(String loanAmount) {
        super.sendKeys(this.loanAmountField, loanAmount);
    }

    public void setDownPaymentField(String downPayment) {
        super.sendKeys(this.downPaymentField, downPayment);
    }

    public void chooseRandomFromAccount() {
        super.chooseRandomSelect(this.fromAccountSelect);
    }
}
