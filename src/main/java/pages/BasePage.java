package pages;

import com.aventstack.extentreports.Status;
import interfaces.IStaleElement;
import org.openqa.selenium.*;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import reports.ReportsListener;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Random;

public class BasePage {
    protected WebDriver driver;
    protected WebDriverWait wait;

    public BasePage(WebDriver driver) {
        this.wait = new WebDriverWait(driver, Duration.ofSeconds(10));
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public void sendKeys(WebElement element,String value){
        this.wait.until(ExpectedConditions.elementToBeClickable(element)).sendKeys(value);
        ReportsListener.getNode().log(Status.PASS, "insert  " + value);
    }

    public void click(WebElement element,String name){
        this.wait.until(ExpectedConditions.elementToBeClickable(element)).click();
        ReportsListener.getNode().log(Status.PASS, "click  " + name);
    }

    public void chooseSelectValue(WebElement selectElement, String value) {
        this.wait.until(ExpectedConditions.elementToBeClickable(selectElement));
        Select select = new Select(selectElement);
        this.wait.until(ExpectedConditions.visibilityOfNestedElementsLocatedBy(selectElement, By.tagName("option")));
        this.wait.until(ExpectedConditions.textToBePresentInElement(selectElement, value));
        select.selectByValue(value);
        ReportsListener.getNode().log(Status.PASS, value + " selected" );
    }

    public void chooseRandomSelect(WebElement selectElement) {
        this.wait.until(ExpectedConditions.elementToBeClickable(selectElement));
        Select select = new Select(selectElement);
        Random random = new Random();

        int index = random.nextInt(select.getOptions().size());

        select.selectByIndex(index);
        String value = select.getOptions().get(index).getText();
        this.wait.until(ExpectedConditions.textToBePresentInElement(selectElement, value));
        ReportsListener.getNode().log(Status.PASS, "select the " + value + " option");
    }

    public String getElementText(WebElement element) {
       return this.wait.until(ExpectedConditions.visibilityOf(element)).getText();
    }

    public void logPassValues(String ...values){
        ReportsListener.getNode().log(Status.PASS, Arrays.asList(values).toString());
    }

    public void findStaleElement(IStaleElement func) {
       int startTime = Calendar.getInstance().get(Calendar.SECOND);
        while (Calendar.getInstance().get(Calendar.SECOND) - startTime > 10) {
            try {
                func.run();
                break;
            } catch (StaleElementReferenceException e) {
                System.out.println(e.getCause().toString());
            }
        }
    }

    public void openNewTab(String url) {
        JavascriptExecutor js = (JavascriptExecutor)this.driver;
        js.executeScript("window.open('" + url + "');");
        ArrayList<String> tabs = new ArrayList<> (driver.getWindowHandles());
        driver.switchTo().window(tabs.get(tabs.size()-1));
    }

    public void switchToParentTab() {
        ArrayList<String> tabs = new ArrayList<> (driver.getWindowHandles());
        driver.switchTo().window(tabs.get(0));
    }
}


