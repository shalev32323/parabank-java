package pages;

import com.aventstack.extentreports.Status;
import enums.LoginFields;
import interfaces.IFormPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import reports.ReportsListener;
import java.util.List;

public class LoginPage extends BasePage implements IFormPage<LoginFields> {
    @FindBy(css = "#loginPanel p a")
    private List<WebElement> registerLink;

    @FindBy(css = "[type = 'submit'][value= 'Log In']")
    private WebElement loginBtn;

    @FindBy(className = "title")
    private WebElement pageTitle;

    @FindBy(className = "error")
    private WebElement errorMessage;

    public LoginPage(WebDriver driver) {
        super(driver);
    }

    public RegisterPage openRegisterPage() {
        super.click(this.registerLink.get(1), "register link");
        return new RegisterPage(driver);
    }

    public String getPageTitle() {
        return super.getElementText(this.pageTitle);
    }

    public String getErrorMessage() {
        return super.getElementText(this.errorMessage);
    }

    public HomePage clickLoginBtn () {
        super.click(this.loginBtn, "login button");
        return new HomePage(driver);
    }

    @Override
    public void clearField(LoginFields registerFields) {
        wait.until(ExpectedConditions.elementToBeClickable(By.name(registerFields.fieldName))).clear();
        ReportsListener.getNode().log
                (Status.PASS, "clear " + registerFields.name() + " field");
    }

    @Override
    public void fillField(LoginFields key, String value) {
        wait.until(ExpectedConditions.elementToBeClickable(By.name(key.fieldName))).sendKeys(value);
        ReportsListener.getNode().log
                (Status.PASS, "insert " + value + " to " + key.name() + " field");
    }
}
