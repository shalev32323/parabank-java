package pages;

import com.aventstack.extentreports.Status;
import enums.BillPayeeFields;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import reports.ReportsListener;

public class BillPayPage extends BasePage {
    @FindBy(name = "verifyAccount")
    private WebElement verifyAccountField;

    @FindBy(name = "amount")
    private WebElement amountField;

    @FindBy(name = "fromAccountId")
    private WebElement fromAccountSelect;

    @FindBy(css = "[type = 'submit'][value= 'Send Payment']")
    private WebElement sendPaymentBtn;

    public BillPayPage(WebDriver driver) {
        super(driver);
    }

    public void setPayeeDetails (BillPayeeFields fields, String value) {
        String locator = "payee" + fields.field;
        super.wait.until(ExpectedConditions.elementToBeClickable
                (By.name(locator))).sendKeys(value);
        ReportsListener.getNode().log(Status.PASS, "insert  " + value);
    }

    public String getErrorMessage (BillPayeeFields fields) {
        String locator = "!validationModel" + fields.field;
        return super.wait.until(ExpectedConditions.visibilityOfElementLocated(By.name(locator))).getText();
    }

    public void setAmountField(String value) {
        super.sendKeys(this.amountField, value);
    }

    public void setVerifyAccountField (String value) {
        super.sendKeys(this.verifyAccountField, value);
    }

    public void chooseFromAccount (String value) {
        super.chooseSelectValue(this.fromAccountSelect, value);
    }

    public void clickSendPayment() {
        super.click(this.sendPaymentBtn, "send payment");
    }
}
