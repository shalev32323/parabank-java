package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class OpenNewAccountPage extends BasePage {
    @FindBy(id = "type")
    private WebElement accountTypeSelect;

    @FindBy(id = "fromAccountId")
    private WebElement fromAccountSelect;

    @FindBy(css = "[type = 'submit'][value= 'Open New Account']")
    private WebElement openNewAccountBtn;

    @FindBy(id = "newAccountId")
    private WebElement newAccountId;

    @FindBy(className = "title")
    private WebElement pageTitle;

    public OpenNewAccountPage(WebDriver driver) {
        super(driver);
    }

    public void chooseAccountType() {
        super.chooseRandomSelect(this.accountTypeSelect);
    }

    public void chooseFromAccount(String value) {
        super.chooseSelectValue(this.fromAccountSelect, value);
    }

    public String getPageTitle() {
        super.wait.until(ExpectedConditions.visibilityOf(this.newAccountId));
        return super.getElementText(this.pageTitle);
    }

    public void chooseFromAccount() {
        super.findStaleElement(() -> super.chooseRandomSelect(this.fromAccountSelect));
    }

    public void clickOpenNewAccount() {
        super.click(this.openNewAccountBtn, "open a new account");
    }

    public String getNewAccountId() {
       return super.getElementText(this.newAccountId);
    }
}
