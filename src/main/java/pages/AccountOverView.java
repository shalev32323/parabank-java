package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import java.util.List;

public class AccountOverView extends BasePage {

    @FindBy(css = "#accountTable tbody [ng-repeat='account in accounts'] td a")
    private List<WebElement> accountTableId;

    @FindBy(css = "#accountTable tbody [ng-repeat='account in accounts'] td:last-child")
    private List<WebElement> accountTableAmount;

    public AccountOverView(WebDriver driver) {
        super(driver);
    }

    public String getLastAccountId() {
        return super.getElementText(this.accountTableId.get(this.accountTableId.size() -1));
    }

    public String getAccountAmount(String accountId) {
        String amount = null;
        for (int i = 0; i < this.accountTableId.size(); i++) {
            if (super.getElementText(this.accountTableId.get(i)).equals(accountId)) {
                amount = super.getElementText(this.accountTableAmount.get(i));
            }
        }
        return amount;
    }

    public String getLastAccountAmount() {
        return super.getElementText(this.accountTableAmount.get(this.accountTableAmount.size() -1));
    }
}
