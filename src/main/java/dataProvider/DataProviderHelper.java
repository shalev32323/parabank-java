package dataProvider;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import enums.RegisterFields;
import org.testng.annotations.DataProvider;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Iterator;


public class DataProviderHelper {

    @DataProvider(name = "registerFields")
    public static Iterator<Object[]> data() throws FileNotFoundException {
        final String PATH_REGISTER_VALUES = "src/main/resources/json_files/negative_register.json";
        ArrayList<Object[]> data = new ArrayList<>();

        FileReader reader = new FileReader(PATH_REGISTER_VALUES);
        JsonObject jsonObject = JsonParser.parseReader(reader).getAsJsonObject();

        for (var entry : jsonObject.entrySet()) {
            RegisterFields field = Enum.valueOf(RegisterFields.class, entry.getKey());
            JsonArray values = entry.getValue().getAsJsonArray();
            for (JsonElement value :  values) {
                data.add(new Object[] {field, value.getAsString()});
            }
        }
        return data.iterator();
    }
}
