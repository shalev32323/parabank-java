package enums;

public enum LoginFields {
    USER_NAME("username"),
    PASSWORD("password");

    public final String fieldName;

    LoginFields(String fieldName) {
        this.fieldName = fieldName;
    }

}
