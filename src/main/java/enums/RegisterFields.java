package enums;

public enum RegisterFields {
    FIRST_NAME("customer.firstName"),
    LAST_NAME("customer.lastName"),
    ADDRESS("customer.address.street"),
    CITY("customer.address.city"),
    STATE("customer.address.state"),
    ZIP_CODE("customer.address.zipCode"),
    PHONE("customer.phoneNumber"),
    SSN("customer.ssn"),
    USER_NAME("customer.username"),
    PASSWORD("customer.password"),
    REPEATED_PASSWORD("repeatedPassword");

    public final String fieldName;

    RegisterFields(String fieldName) {
        this.fieldName = fieldName;
    }
}
