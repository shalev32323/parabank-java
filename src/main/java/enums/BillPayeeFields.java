package enums;

public enum BillPayeeFields {
    PAYEE_NAME(".name"),
    ADDRESS(".address.street"),
    CITY(".address.city"),
    STATE(".address.state"),
    ZIP_CODE(".address.zipCode"),
    PHONE(".phoneNumber"),
    ACCOUNT(".accountNumber");

    public final String field;

    BillPayeeFields(String field) {
        this.field = field;
    }
}
