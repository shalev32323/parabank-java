package enums;

public enum SuccessMessages {
    REGISTER_MESSAGE("Your account was created successfully. You are now logged in.");

    public final String message;

    SuccessMessages(String message) {
        this.message = message;
    }
}
