package enums;

public enum PagesTitles {
    ACCOUNT_OPENED("Account Opened!"),
    PANEL_TITLE("Account Services"),
    HOME_TITLE("Welcome user"),
    BILL_PAY_TITLE("title"),
    LOAN_REQUEST_TITLE("title"),
    LOGIN_TITLE("Customer Login"),
    OPEN_NEW_ACCOUNT_TITLE("title"),
    REGISTER_TITLE("Signing up is easy!"),
    TRANSFER_FUNDS_TITLE("title"),
    ACCOUNT_OVERVIEW("Accounts Overview");

    public final String title;

    PagesTitles(String title) {
        this.title = title;
    }
}
