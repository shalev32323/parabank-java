package utils;

import com.aventstack.extentreports.Status;
import interfaces.IStaleElement;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import reports.ReportsListener;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Random;

public class ElementUtils {

    public static void sendKeys(WebElement element, String value){
        WaitUtils.waitToBeClickable(element).sendKeys(value);
        ReportsListener.getNode().log(Status.PASS, "insert  " + value);
    }

    public static void click(WebElement element,String name){
        WaitUtils.waitToBeClickable(element).click();
        ReportsListener.getNode().log(Status.PASS, "click  " + name);
    }

    public static void chooseSelectValue(WebElement selectElement, String value) {
        WaitUtils.waitToBeClickable(selectElement);
        Select select = new Select(selectElement);
        select.selectByValue(value);
        ReportsListener.getNode().log(Status.PASS, value + " selected" );
    }

    public static void chooseRandomSelect(WebElement selectElement) {
        WaitUtils.waitToBeClickable(selectElement);
        Select select = new Select(selectElement);
        Random random = new Random();

        int index = random.nextInt(select.getOptions().size());

        select.selectByIndex(index);
        String value = select.getOptions().get(index).getText();
        ReportsListener.getNode().log(Status.PASS, "select the " + value + " option");
    }

    public static String getElementText(WebElement element) {
        return WaitUtils.waitToBeVisible(element).getText();
    }

    public static void logPassValues(String ...values){
        ReportsListener.getNode().log(Status.PASS, Arrays.asList(values).toString());
    }

    public static void findStaleElement(IStaleElement func) {
        int startTime = Calendar.getInstance().get(Calendar.SECOND);
        while (Calendar.getInstance().get(Calendar.SECOND) - startTime > 10) {
            try {
                func.run();
                break;
            } catch (StaleElementReferenceException e) {
                System.out.println(e.getCause().toString());
            }
        }
    }

}
