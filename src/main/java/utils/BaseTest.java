package utils;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.asserts.SoftAssert;
import reports.ReportsListener;
import java.util.Objects;

public class BaseTest {
    {
        System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver");
    }

    public static WebDriver driver;
    public static ExtentTest test;
    public static final String BASE_URL = "https://parabank.parasoft.com/parabank/index.htm";
    private SoftAssert softAssert;

    public void initializeReportTest() {
        test = ReportsListener.extent.createTest(getClass().getName());
    }

    public void setUp() {
        driver = new ChromeDriver();
        softAssert = new SoftAssert();
        driver.get(BASE_URL);
        driver.manage().window().maximize();
    }

    public void assertVisible(WebElement element){
        Assert.assertTrue(element.isDisplayed());
        ReportsListener.getNode().log(Status.PASS, "check if the element is visible" );
    }

    public  <T> void assertEquals(T actual, T expected) {
        Assert.assertEquals(actual, expected);
        ReportsListener.getNode().log(Status.PASS,
                "[ " + actual + " ] was equal to [ " + expected +" ]");
    }

    public <T> void softAssertEquals(T actual, T expected) {
        boolean passed = Objects.equals(actual, expected);
        softAssert.assertEquals(actual, expected);
        if (passed) {
            ReportsListener.getNode().log(Status.PASS,
                    "checked if the [ " + actual + " ] was equal to [ " + expected +" ]");
        } else {
            ReportsListener.getNode().log(Status.FAIL,
                    "failed because [ " + actual + " ] was not equal to [ " + expected + " ]");
        }
    }

    public void assertAllSoft() {
        softAssert.assertAll();
    }

    public void terminate() throws InterruptedException {
        driver.quit();
    }
}

