package utils;

import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;

import java.util.Calendar;
import java.util.function.BooleanSupplier;

public class WaitUtils {
    public static final int TIME_OUT = 10;

    public static boolean waitForCondition(BooleanSupplier test, int timeout) {
        int startTime = Calendar.getInstance().get(Calendar.SECOND);
        while (Calendar.getInstance().get(Calendar.SECOND) - startTime > timeout) {
           if (test.getAsBoolean()) {
               return true;
           }
        }
        return false;
    }

    public static boolean waitForCondition(BooleanSupplier test) {
       return waitForCondition(test, TIME_OUT);
    }

    public static WebElement waitToBeVisible(WebElement element) {
       if(waitForCondition(element::isDisplayed)) return element;
       throw new TimeoutException("element is not displayed");
    }

    public static WebElement waitToBeClickable(WebElement element) {
        if(element.isDisplayed() && element.isEnabled()) return element;
        throw new TimeoutException("element is not clickable");
    }
}
