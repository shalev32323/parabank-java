package utils;

import com.google.gson.*;

import javax.annotation.Nonnull;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class JsonUtils {

    private static<T extends Enum<T>> HashMap<T, String> JsonObjectToMap(JsonObject jsonObject, Class<T> en) throws JsonParseException {
        HashMap<T, String> object = new HashMap<>();
        for (var pair : jsonObject.entrySet()) {
            try {
                T key = Enum.valueOf(en, pair.getKey());
                String value =  pair.getValue().getAsString();
                object.put(key, value);
            } catch (Throwable e) {
                throw new JsonParseException("failes to parse object with key " + pair.getKey() + ".", e);
            }
        }

        return object;
    }

    public static <T extends Enum<T>> HashMap<T, String> loadJsonObjectAsMap(String path, Class<T> en) throws IOException {
        FileReader reader = new FileReader(path);
        JsonObject jsonObject = JsonParser.parseReader(reader).getAsJsonObject();
        return JsonObjectToMap(jsonObject, en);
    }
}


