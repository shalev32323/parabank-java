package negativeTests;

import commonSteps.CommonSteps;
import dataProvider.DataProviderHelper;
import enums.PagesTitles;
import enums.RegisterFields;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pages.LoginPage;
import pages.RegisterPage;
import utils.JsonUtils;

import java.io.IOException;
import java.util.*;

public class NegativeRegister extends MethodLevelTest {
    final String PATH_ERRORS = "src/main/resources/json_files/errors_required.json";
    final String PATH_REGISTER_VALUES = "src/main/resources/json_files/negative_register.json";
    final String PATH_USER_DETAILS = "src/main/resources/json_files/user_details.json";
    public Map<RegisterFields, String> errors;
    public RegisterPage registerPage;
    public LoginPage loginPage;

    @BeforeMethod(groups = "negative")
    public void beforeRegister() throws IOException {
        errors = JsonUtils.loadJsonObjectAsMap(PATH_ERRORS, RegisterFields.class);
        loginPage = new LoginPage(driver);
        registerPage = loginPage.openRegisterPage();
    }

    @Test(dataProvider = "registerFields", groups = "negative", dataProviderClass = DataProviderHelper.class)
    public void validateRegisterFields(RegisterFields field, String value) throws IOException {
        String id = String.valueOf(Calendar.getInstance().getTimeInMillis());

        CommonSteps.fillFields(RegisterFields.class, PATH_USER_DETAILS, registerPage);
        registerPage.fillField(RegisterFields.USER_NAME, id);
        registerPage.clearField(field);
        registerPage.fillField(field, value);
        registerPage.clickRegister();
        softAssertEquals(registerPage.getErrorMessage(field), errors.get(field));
        assertEquals(registerPage.getPageTitle(), PagesTitles.REGISTER_TITLE.title);
        assertAllSoft();
    }

    @Test(groups = "negative")
    public void registerWithNoValues () {
        registerPage.clickRegister();
        for (Map.Entry<RegisterFields, String> error : errors.entrySet()) {
            softAssertEquals(registerPage.getErrorMessage(error.getKey()), error.getValue());
        }
        assertEquals(registerPage.getPageTitle(), PagesTitles.REGISTER_TITLE.title);
        assertAllSoft();
    }
}
