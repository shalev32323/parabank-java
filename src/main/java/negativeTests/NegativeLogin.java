package negativeTests;

import commonSteps.CommonSteps;
import enums.LoginFields;
import enums.PagesTitles;
import enums.RegisterFields;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pages.HomePage;
import pages.LoginPage;
import java.io.IOException;
import java.util.*;

public class NegativeLogin extends MethodLevelTest {

    final String PATH = "src/main/resources/json_files/user_details.json";
    Map<RegisterFields, String> userDetails;
    LoginPage loginPage;
    public final String LOGIN_ERROR_MESSAGE = "Please enter a username and password.";

    @BeforeMethod(groups = "negative")
    public void registerAndLogOut() throws IOException {
        userDetails = CommonSteps.register(PATH);
        new HomePage(driver).clickLogOut();
    }

    @Test(groups = "negative")
    public void loginWithGoodUserNameAndWrongPassword() {
        loginPage = new LoginPage(driver);
        loginPage.fillField(LoginFields.USER_NAME, userDetails.get(RegisterFields.USER_NAME));
        loginPage.fillField(LoginFields.PASSWORD, userDetails.get(RegisterFields.USER_NAME));
        loginPage.clickLoginBtn();
        assertEquals(loginPage.getPageTitle(), PagesTitles.LOGIN_TITLE.title);
    }

    @Test(groups = "negative")
    public void loginWithNoExistingUser() {
        loginPage = new LoginPage(driver);
        String userName = String.valueOf(Calendar.getInstance().getTimeInMillis());
        String password = String.valueOf(Calendar.getInstance().getTime());
        loginPage.fillField(LoginFields.USER_NAME, userName);
        loginPage.fillField(LoginFields.PASSWORD, password);
        loginPage.clickLoginBtn();
        assertEquals(loginPage.getPageTitle(), PagesTitles.LOGIN_TITLE.title);
    }

    @Test(groups = "negative")
    public void loginWithGoodPasswordAndWrongUserName() {
        loginPage = new LoginPage(driver);
        loginPage.fillField(LoginFields.USER_NAME, userDetails.get(RegisterFields.PASSWORD));
        loginPage.fillField(LoginFields.PASSWORD, userDetails.get(RegisterFields.PASSWORD));
        loginPage.clickLoginBtn();
        assertEquals(loginPage.getPageTitle(), PagesTitles.LOGIN_TITLE.title);
    }

    @Test(groups = "negative")
    public void loginWithNoValues () {
        loginPage = new LoginPage(driver);
        loginPage.clickLoginBtn();
        softAssertEquals(loginPage.getErrorMessage(),LOGIN_ERROR_MESSAGE);
        assertEquals(new HomePage(driver).getPanelTitle(), PagesTitles.LOGIN_TITLE.title);
        assertAllSoft();
    }
}
