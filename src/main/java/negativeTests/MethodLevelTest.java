package negativeTests;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import utils.BaseTest;

public class MethodLevelTest extends BaseTest {

    @BeforeClass(groups = "set_up_negative")
    @Override
    public void initializeReportTest() {
        super.initializeReportTest();
    }

    @BeforeMethod(groups = "set_up_negative")
    @Override
    public void setUp(){
        super.setUp();
    }

    @AfterMethod(groups = "set_up_negative")
    @Override
    public void terminate() throws InterruptedException {
        super.terminate();
    }
}
