package interfaces;

@FunctionalInterface
public interface IStaleElement {
    void run();
}
