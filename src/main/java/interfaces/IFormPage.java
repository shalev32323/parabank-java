package interfaces;

public interface IFormPage<T extends Enum<T>> {

    void fillField(T key, String value);

    void clearField(T key);
}
