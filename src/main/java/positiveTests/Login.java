package positiveTests;

import commonSteps.CommonSteps;
import enums.LoginFields;
import enums.PagesTitles;
import enums.RegisterFields;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pages.HomePage;
import pages.LoginPage;
import java.io.IOException;
import java.util.Map;

public class Login extends ClassLevelTest {

    final String PATH = "src/main/resources/json_files/user_details.json";
    Map<RegisterFields, String> userDetails;
    LoginPage loginPage;

    @BeforeMethod(groups = "positive")
    public void registerAndLogOut() throws IOException {
        userDetails = CommonSteps.register(PATH);
        new HomePage(driver).clickLogOut();
    }

    @Test(groups = "positive")
    public void login() {
        loginPage = new LoginPage(driver);
        loginPage.fillField(LoginFields.USER_NAME,userDetails.get(RegisterFields.USER_NAME));
        loginPage.fillField(LoginFields.PASSWORD,userDetails.get(RegisterFields.PASSWORD));
        loginPage.clickLoginBtn();
        assertEquals(new HomePage(driver).getPanelTitle(), PagesTitles.PANEL_TITLE.title);
        softAssertEquals(loginPage.getPageTitle(), PagesTitles.ACCOUNT_OVERVIEW.title);
        assertAllSoft();
    }

}
