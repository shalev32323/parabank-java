package positiveTests;

import commonSteps.CommonSteps;
import enums.PagesTitles;
import enums.RegisterFields;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pages.AccountOverView;
import pages.HomePage;
import pages.OpenNewAccountPage;
import java.io.IOException;
import java.util.Map;

public class OpenNewAccount extends ClassLevelTest {

    final String PATH = "src/main/resources/json_files/user_details.json";
    public Map<RegisterFields, String> userDetails;
    public HomePage homePage;
    public AccountOverView accountOverView;
    final String EXPECTED_AMOUNT = "$100.00";

    @BeforeMethod(groups = "positive")
    public void registerAndLogin() throws IOException {
        userDetails = CommonSteps.register(PATH);
        new HomePage(driver).clickLogOut();
        homePage  = CommonSteps.login
                (userDetails.get(RegisterFields.USER_NAME),userDetails.get(RegisterFields.PASSWORD));
    }

    @Test(groups = "positive")
    public void openNewAccount() throws InterruptedException {
        OpenNewAccountPage openNewAccountPage = homePage.clickOpenNewAccount();
        openNewAccountPage.chooseAccountType();
        openNewAccountPage.chooseFromAccount();
        openNewAccountPage.clickOpenNewAccount();
        softAssertEquals(openNewAccountPage.getPageTitle(), PagesTitles.ACCOUNT_OPENED.title);
        String newAccountId = openNewAccountPage.getNewAccountId();
        accountOverView = homePage.clickAccountOverview();
        Thread.sleep(1000);
        assertEquals(accountOverView.getLastAccountId(), newAccountId);
        assertEquals(accountOverView.getLastAccountAmount(), EXPECTED_AMOUNT);
        assertAllSoft();
    }

}
