package positiveTests;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import utils.BaseTest;

public class ClassLevelTest extends BaseTest {

    @BeforeClass
    @Override
    public void setUp(){
        super.initializeReportTest();
        super.setUp();
    }

    @AfterClass
    @Override
    public void terminate() throws InterruptedException {
        Thread.sleep(5000);
        super.terminate();
    }
}
