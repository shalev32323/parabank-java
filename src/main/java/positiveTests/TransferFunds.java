package positiveTests;

import commonSteps.CommonSteps;
import enums.RegisterFields;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pages.AccountOverView;
import pages.HomePage;
import pages.TransferFundsPage;
import java.io.IOException;
import java.util.Map;

public class TransferFunds extends ClassLevelTest {

    final String PATH = "src/main/resources/json_files/user_details.json";
    public Map<RegisterFields, String> userDetails;
    public HomePage homePage;
    public TransferFundsPage transferFundsPage;
    public AccountOverView accountOverView;
    public String fromAccount;
    public String toAccount;
    public String fromAccountAmount;
    public String toAccountAmount;

    @BeforeMethod(groups = "positive")
    public void registerAndLogin() throws IOException {
        userDetails = CommonSteps.register(PATH);
        new HomePage(driver).clickLogOut();
        homePage  = CommonSteps.login
                (userDetails.get(RegisterFields.USER_NAME),userDetails.get(RegisterFields.PASSWORD));
        homePage.clickOpenNewAccount();
        fromAccount = CommonSteps.openNewAccount();
        homePage.clickOpenNewAccount();
        toAccount = CommonSteps.openNewAccount();
    }

    @Test(groups = "positive")
    public void transferFunds() throws InterruptedException {
        final String AMOUNT = "100";
        String startedAmountFromAccount;
        String startedAmountToAccount;

        accountOverView = homePage.clickAccountOverview();
        Thread.sleep(1000);
        startedAmountFromAccount = accountOverView.getAccountAmount(fromAccount);
        startedAmountToAccount = accountOverView.getAccountAmount(toAccount);
        transferFundsPage = homePage.clickTransferFunds();
        Thread.sleep(500);
        transferFundsPage.setAmountField(AMOUNT);
        transferFundsPage.chooseFromAccount(fromAccount);
        transferFundsPage.chooseToAccount(toAccount);
        transferFundsPage.clickTransfer();
        accountOverView = homePage.clickAccountOverview();
        Thread.sleep(1000);
        fromAccountAmount = accountOverView.getAccountAmount(fromAccount);
        toAccountAmount = accountOverView.getAccountAmount(toAccount);
        assertEquals(fromAccountAmount, expectedFromAmount(startedAmountFromAccount, AMOUNT));
        assertEquals(toAccountAmount, expectedToAmount(startedAmountToAccount, AMOUNT));
    }

    private static String expectedFromAmount(String account, String amount) {
        String dollarSign = "$";
        double account1 = Double.parseDouble(account.substring(1));
        double amount1 = Double.parseDouble(amount);
        return dollarSign + (account1 - amount1) + "0";
    }

    private static String expectedToAmount(String account, String amount) {
        String dollarSign = "$";
        double account1 = Double.parseDouble(account.substring(1));
        double amount1 = Double.parseDouble(amount);
        return dollarSign + (account1 + amount1) + "0";
    }

}
