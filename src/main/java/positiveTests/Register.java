package positiveTests;

import enums.PagesTitles;
import enums.RegisterFields;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pages.LoginPage;
import pages.HomePage;
import pages.RegisterPage;
import utils.JsonUtils;
import java.io.IOException;
import java.util.Calendar;
import java.util.Map;

public class Register extends ClassLevelTest {

    final String PATH = "src/main/resources/json_files/user_details.json";
    Map<RegisterFields, String> userDetails;

    @BeforeMethod(groups = "positive")
    public void loadUserDetails() throws IOException {
        userDetails = JsonUtils.loadJsonObjectAsMap(PATH, RegisterFields.class);
    }

    @Test(groups = "positive")
    public void register(){
        RegisterPage registerPage = new LoginPage(driver).openRegisterPage();
        long id = Calendar.getInstance().getTimeInMillis();

        for (var field : userDetails.entrySet()) {
            if (field.getKey().name().equals("USER_NAME"))
                registerPage.fillField(field.getKey(),field.getValue() + id);
            else registerPage.fillField(field.getKey(), field.getValue());
        }
        HomePage homePage = registerPage.clickRegister();
        assertEquals(homePage.getPanelTitle(), PagesTitles.PANEL_TITLE.title);
        softAssertEquals(homePage.getPageTitle(), PagesTitles.HOME_TITLE.title + id);
        assertAllSoft();
    }
}
