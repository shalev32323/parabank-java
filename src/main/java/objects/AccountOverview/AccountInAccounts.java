package objects.AccountOverview;

import pages.Component;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import utils.ElementUtils;

public class AccountInAccounts extends Component<WebElement> {
    @FindBy(css = "td a")
    private WebElement accountId;

    @FindBy(css = "[ng-repeat='account in accounts']:nth-child(1)")
    private WebElement accountBalance;

    @FindBy(css = "[ng-repeat='account in accounts']:nth-child(2)")
    private WebElement availableAmount;

    public AccountInAccounts(WebElement root) {
        super(root);
    }

    public String getAccountBalance() {
        return ElementUtils.getElementText(this.accountBalance);
    }

    public String getAccountId() {
        return ElementUtils.getElementText(this.accountId);
    }

    public String getAccountAmount() {
        return ElementUtils.getElementText(this.availableAmount);
    }
}
