package objects.AccountOverview;

import pages.Component;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import java.util.List;

public class AccountOverviewTable extends Component<WebElement> {

    @FindBy(css = "[ng-repeat='account in accounts']")
    private List<WebElement> rows;


    public AccountOverviewTable(WebElement root) {
        super(root);
    }


    public AccountInAccounts getRow(int accountId) {
        for (WebElement row : rows) {
            AccountInAccounts accountInAccounts = new AccountInAccounts(row);
            if (accountInAccounts.getAccountId().equals(accountId)) {
                return accountInAccounts;
            }
        }
        return null;
    }

}
