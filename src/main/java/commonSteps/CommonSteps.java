package commonSteps;

import enums.LoginFields;
import enums.RegisterFields;
import interfaces.IFormPage;
import pages.HomePage;
import pages.LoginPage;
import pages.OpenNewAccountPage;
import pages.RegisterPage;
import utils.BaseTest;
import utils.JsonUtils;
import java.io.IOException;
import java.util.Calendar;
import java.util.Map;

public class CommonSteps extends BaseTest {

    public static Map<RegisterFields, String> register(String PATH) throws IOException {
        Map<RegisterFields, String> userDetails;
        userDetails = JsonUtils.loadJsonObjectAsMap(PATH, RegisterFields.class);
        RegisterPage registerPage = new LoginPage(driver).openRegisterPage();
        Calendar calendar = Calendar.getInstance();
        long id = calendar.getTimeInMillis();

        for (var field : userDetails.entrySet()) {
            if (field.getKey().name().equals("USER_NAME")) {
                field.setValue(field.getValue() + id);
                registerPage.fillField(field.getKey(), field.getValue());
            }
            else registerPage.fillField(field.getKey(), field.getValue());
        }

        registerPage.clickRegister();
        return userDetails;
    }

    public static HomePage login(String userName, String password) {
        LoginPage loginPage = new LoginPage(driver);
        loginPage.fillField(LoginFields.USER_NAME, userName);
        loginPage.fillField(LoginFields.PASSWORD, password);
        return loginPage.clickLoginBtn();
    }

    public static String openNewAccount() {
        OpenNewAccountPage openNewAccountPage = new OpenNewAccountPage(driver);
        openNewAccountPage.chooseAccountType();
        openNewAccountPage.clickOpenNewAccount();
        return openNewAccountPage.getNewAccountId();
    }

    public static <T extends Enum<T>> void fillFields(Class<T> fieldsType, String PATH, IFormPage<T> page) throws IOException {
        Map<T, String> userDetails;
        userDetails = JsonUtils.loadJsonObjectAsMap(PATH, fieldsType);

        for (var field : userDetails.entrySet()) {
            page.fillField(field.getKey(), field.getValue());
        }
    }
}
